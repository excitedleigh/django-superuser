from setuptools import find_packages, setup
from os.path import dirname, join

with open(join(dirname(__file__), 'README.rst')) as f:
    long_desc = f.read()

setup(
    name='django-superuser',
    setup_requires=['setuptools_scm>=1.11.1'],
    use_scm_version=True,
    description='Quickly create a superuser in your Django project',
    long_description=long_desc,
    url='https://gitlab.com/abre/django-superuser',
    author='Adam Brenecki',
    author_email='adam@brenecki.id.au',
    license='MIT',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development',
    ],
    keywords='django superuser tools',
    py_modules=['django_superuser'],
    entry_points={
        'console_scripts': ['django-superuser=django_superuser:main'],
    }
)
