#!/usr/bin/env python
from django import setup
setup()
from django.contrib.auth import get_user_model

def main():
    User = get_user_model()
    first_superuser = User.objects.filter(
        is_superuser=True, is_staff=True).order_by('pk').first()
    if first_superuser is None:
        print("No staff superusers already existing, making a new one...")
        first_superuser = User.objects.create(
            username='user', is_superuser=True, is_staff=True)
    first_superuser.set_password('password')
    first_superuser.save()

    print("Username:", first_superuser.username)
    print("Password: password")

if __name__ == "__main__":
    main()