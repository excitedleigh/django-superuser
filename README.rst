django-superuser
================

A simple tool that takes the first staff superuser in your Django project, 
creating one if it does not exist, and sets its password to 'password'.

Installation
------------

For the time being, until I publish this on PyPI::

    pip install git+https://gitlab.com/abre/django-superuser.git

Usage
-----

::
    $ export DJANGO_SETTINGS_MODULE=myapp.settings
    $ django-superuser
    No staff superusers already existing, making a new one...
    Username: user
    Password: password